package com.timkaragosian.danielmaloneecommerceapp.model

data class Product(
    val title: String,
    val photoUrl: String,
    val price: Double
)